package net.codejava;

public class Knight {
	
	private int baseHP;
	private int wp;

	public Knight(int baseHP, int wp) {
		
		//Check baseHP
		if(baseHP < 99 || baseHP > 999 || wp < 0 || wp > 3)
		{
			System.out.println("Knight's data is out of range !!!");
			System.out.println();
		}
		else 
		{
			this.baseHP = baseHP;
			this.wp = wp;
		}
	}
	
	public int getBaseHP() {
		return baseHP;
	}
	
	public int getwp() {
		return wp;
	}
	
	public int getRealHP() {
		if(wp == 0) return (baseHP / 10);
		else return baseHP; //Because there is no requirement for 2,3 so I assume that it is the same as 1
	}
}
