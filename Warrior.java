package net.codejava;

public class Warrior {

	private int baseHP;
	private int wp;

	public Warrior(int baseHP, int wp) {
		
		//Check baseHP
		if(baseHP < 1 || baseHP > 888 || wp < 0 || wp > 3)
		{
			System.out.println("Warrior's data is out of range !!!");
			System.out.println();
		}
		else 
		{
			this.baseHP = baseHP;
			this.wp = wp;
		}
	}
	
	public int getBaseHP() {
		return baseHP;
	}
	
	public int getwp() {
		return wp;
	}
	
	public int getRealHP() {
		if(wp == 0) return (baseHP / 10);
		else return baseHP; //Because there is no requirement for 2,3 so I assume that it is the same as 1
	}
}
